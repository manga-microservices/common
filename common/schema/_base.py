from pydantic import BaseModel
from pydantic.generics import GenericModel

from common.utils import snake_to_camel


class BaseSchema(BaseModel):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        alias_generator = snake_to_camel


class GenericSchema(GenericModel):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True
        alias_generator = snake_to_camel
