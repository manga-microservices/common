from ._base import BaseSchema, GenericSchema

__all__ = ["BaseSchema", "GenericSchema"]
