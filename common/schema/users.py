import datetime
import uuid

from common.schema import BaseSchema


class UserSchema(BaseSchema):
    id: uuid.UUID
    username: str
    created_at: datetime.datetime
