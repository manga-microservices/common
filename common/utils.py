import abc
import uuid
from typing import Generic, Iterable, TypeVar

_TModel = TypeVar("_TModel")
_TType = TypeVar("_TType")


def snake_to_camel(name: str) -> str:
    first, *rest = name.split("_")
    return first + "".join(map(str.capitalize, rest))


def filter_uuids(uuids: Iterable[uuid.UUID | str]) -> Iterable[uuid.UUID]:
    for id_ in uuids:
        if not isinstance(id_, uuid.UUID):
            try:
                id_ = uuid.UUID(id_)
            except ValueError:
                continue
        yield id_


class OrmMixin(Generic[_TModel, _TType]):
    @classmethod
    @abc.abstractmethod
    def from_orm(cls, model: _TModel) -> _TType:
        raise NotImplementedError

    @classmethod
    def from_orm_optional(cls, model: _TModel | None) -> _TType | None:
        if model is None:
            return None
        return cls.from_orm(model)

    @classmethod
    def from_orm_list(cls, models: Iterable[_TModel]) -> list[_TType]:
        return [cls.from_orm(model) for model in models]
