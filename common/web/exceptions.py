from starlette import status
from starlette.requests import Request
from starlette.responses import JSONResponse, Response

from common.schema import BaseSchema


class APIException(Exception):
    class Schema(BaseSchema):
        class Config:
            title = "APIException"

        code: str
        detail: str

    status_code: int = status.HTTP_500_INTERNAL_SERVER_ERROR
    code: str = "INTERNAL_SERVER_ERROR"
    detail: str = "Internal Server Error"

    def __init__(
        self,
        detail: str | None = None,
        code: str | None = None,
        status_code: int | None = None,
    ):
        self.detail = detail or self.detail
        self.code = code or self.code
        self.status_code = status_code or self.status_code


class BadRequest(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    code = "BAD_REQUEST"
    detail = "Bad request"


class Unauthorized(APIException):
    status_code = status.HTTP_401_UNAUTHORIZED
    code = "UNAUTHORIZED"
    detail = "Unauthorized"


class NotFound(APIException):
    status_code = status.HTTP_404_NOT_FOUND
    code = "NOT_FOUND"
    detail = "Entity not found"


async def api_exception_handler(request: Request, exception: APIException) -> Response:
    return JSONResponse(
        {
            "code": exception.code,
            "detail": exception.detail,
        },
        status_code=exception.status_code,
    )
